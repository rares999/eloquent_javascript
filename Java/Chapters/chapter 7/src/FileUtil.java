import java.io.*;
import java.nio.charset.StandardCharsets;

public class FileUtil {
    public void writeFile(File file, String content) {
        try (FileOutputStream fop = new FileOutputStream(file)) {

            if (!file.exists()) {
                file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void readFile(String fileName) {
        try (FileInputStream fis = new FileInputStream(new File(fileName))) {
            int content;
            while ((content = fis.read()) != -1) {

                System.out.print((char) content);
            }
            System.out.println();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void appendFile(File file, String content) {
        try (FileOutputStream fop = new FileOutputStream(file, true)) {

            if (!file.exists()) {
                file.createNewFile();
            }

            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            fop.flush();
            fop.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public byte[] readToArray(String fileName) {
        try (FileInputStream fis = new FileInputStream(new File(fileName))) {
            int content;
            byte[] contentToBytes = new byte[fis.available()];
            fis.read(contentToBytes);
            return contentToBytes;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void readLineByLine(String fileName) {
        try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(new File(fileName)))) {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(bis, StandardCharsets.UTF_8));
            String content;
            int index = 0;
            while ((content = reader.readLine()) != null) {
                System.out.print("Line " + index + " :");
                System.out.println(content);
                index++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public int fileSize(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            return fis.available();
        } catch (IOException e) {
            return 0;
        }
    }
}
