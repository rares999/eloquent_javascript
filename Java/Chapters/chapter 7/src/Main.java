import java.io.File;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        File file = new File("output.txt");
        FileUtil fileUtil = new FileUtil();
        fileUtil.readFile("input.txt");
        fileUtil.writeFile(file, "vreau sa vad daca se scrie ");
        fileUtil.appendFile(file, "ni ce fain");
        fileUtil.readLineByLine("input.txt");
        System.out.println(fileUtil.fileSize(file));
    }
}
