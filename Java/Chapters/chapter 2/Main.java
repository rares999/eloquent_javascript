package com.company;


public class Main {
    static void printChapter2Problems()
    {
        Chapter2 chapter2 = new Chapter2();
        System.out.println("problem1:"+chapter2.computePerimeter(12, 1));
        System.out.print("problem2: ");
        for(int num : chapter2.computeArray(7, 5))
            System.out.print(num + " ");
        System.out.println();
        System.out.println("problem3:"+chapter2.next(24));
        System.out.println("problem4:"+chapter2.rectangleInCircle(8, 6, 5));
        System.out.println("problem5:"+chapter2.isPandigital(112233445566778899L));
        System.out.println("problem6:"+chapter2.helloOrBye("daniela", 0));
        System.out.println("problem7:"+chapter2.mySub(10, 30));
        System.out.println("problem8:"+chapter2.reversedBinaryInteger(10));
        int[] arr = {80, 29, 4, -96, -24, 85,1};
        System.out.print("problem9: ");
        for(int num : chapter2.sortNumsAscending(arr))
            System.out.print(num+" ");
    }
    public static void main(String[] args) {
        printChapter2Problems();
    }
}
