package com.company;


public class Main {
    static void printChapter1Problems()
    {
        Chapter1 chapter1 = new Chapter1();
        System.out.println(chapter1.computePerimeter(12, 1));
        System.out.println(chapter1.computeArray(7, 5));
        System.out.println(chapter1.next(24));
        System.out.println(chapter1.rectangleInCircle(4, 7, 4));
        System.out.println(chapter1.isPandigital(112233445566778899L));
        System.out.println(chapter1.helloOrBye("daniela", 0));
        System.out.println(chapter1.mySub(10, 30));
        System.out.println(chapter1.reversedBinaryInteger(10));
        int[] arr = {80, 29, 4, -96, -24, 85,1};
        for(int num : chapter1.sortNumsAscending(arr))
            System.out.print(num+" ");
    }
    public static void main(String[] args) {
        printChapter1Problems();
    }
}
