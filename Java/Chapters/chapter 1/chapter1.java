package com.company;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Chapter1 {


    /**
     * Create a function that computes the perimeter of a rectangle
     * @param length
     * @param width
     * @return
     */
    int computePerimeter(int length, int width) {
        if (length < 0 || width < 0)
            throw new NumberFormatException("Length and Width cannot be negative");
        return 2 * (length + width);
    }

    double computePerimeter(double length, double width) {
        if (length < 0 || width < 0)
            throw new NumberFormatException("Length and Width cannot be negative");
        return 2 * (length + width);
    }



    /**
     * Create a function that takes two numbers as arguments, number and length and
     *     computes an array of multiples of number until the array length equals the length argument
     * @param number
     * @param length lenght of the array to be created
     * @return array[length] of multiples of number given as argument
     */
    List<Integer> computeArray(int number, int length) {
        List<Integer> list = new ArrayList<>();
        if (length < 1)
            return list;
        int multiple = number;
        for (int i = 0; i < length; i++) {
            list.add(multiple);
            multiple = multiple + number;
        }
        return list;
    }



    /**
     * Create a function that takes an integer as argument and returns the next prime
     *     number. If the number is prime, return the number itself.
     * @param number
     * @return next prime >= number ... for negative number it returns next prime <= number
     */
    int next(int number) {
        boolean isNegative = false;
        if (number < 0) {
            number = -number;
            isNegative = true;
        }
        while (!isPrime(number)) {
            number++;
        }
        return isNegative ? -number : number;
    }

    boolean isPrime(int number) {
        for (int i = 2; i <= Math.sqrt(number); i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }



    /**
     * Create a function that takes three numbers — the width and height of a rectangle,
     *     and the radius of a circle and returns true if the rectangle can fit inside the circle,
     *     false if it cannot.
     * @param width
     * @param height
     * @param radius
     * @return true if rectangle can fit inside the circle, false otherwise
     */
    boolean rectangleInCircle(double width, double height, int radius) {
        if (height < 1 || width < 1)
            throw new NumberFormatException("Length and Width cannot be negative");
        double diagonal = Math.sqrt(width * width + height * height);
        return (2 * radius) >= diagonal;
    }



    /**
     *  A pandigital number is an integer that has among its significant digits each digit at
     *least once. For example, 1234567890 is a pandigital number. Create a function that
     *determines if an integer is a pandigital number or not.
     * @param number
     * @return true if it's panadigital or false otherwise
     */
    boolean isPandigital(Long number) {
        if (number < 1234567890)
            return false;
        Set<Integer> listOfPresence = new HashSet<>();
        while (number > 0) {
            int lastDigit = (int) (number % 10);
            number = number / 10;
            listOfPresence.add(lastDigit);
        }
        return listOfPresence.size() == 10;
    }



    /**
     * Create a function that takes a string name and a number num (either 0 or 7) and
     * return "Hello" + name if num is 7, otherwise return "Bye" + name.
     * @param name
     * @param num 0 for Bye or 7 for Hello
     * @return
     */
    String helloOrBye(String name, int num) {
        if (num != 0 && num != 7)
            throw new NumberFormatException("Number can be only 0 or 7");
        String greetings = num == 7 ? "Hello " : "Bye ";
        name = name.substring(0, 1).toUpperCase() + name.substring(1); //first letter to uppercase
        return greetings + name;
    }



    /**
     * Without using any arithmetic operators such as -, %, /, +, create a function that
     * subtracts one positive integer from another
     * @param x number to substract
     * @param y number from which is substracted x
     * @return x - y
     */
    int mySub(int x, int y) {
        if (x < 0 || y < 0)
            throw new NumberFormatException("Numbers must be positive");
        if (x == 0) {
            return y;
        }
        while (x != 0) {
            int carry = x & (~y);
            y = x ^ y;
            x = carry << 1;
        }
        return y;
    }



    /**
     * Write a function that takes a positive integer n, reverses the binary
     * representation of that integer, and returns the new integer from the reversed
     * binary.
     * @param number positive integer number
     * @return  reversed binary from number
     */
    int reversedBinaryInteger(int number) {
        if (number < 0)
            throw new NumberFormatException("Number must be positive");
        int reversedNumber = 0;
        while (number != 0) {
            reversedNumber = (reversedNumber << 1) + number % 2;
            number /= 2;
        }
        return reversedNumber;
    }

    /**
     * Create a method that takes an array of integers and returns a new array, sorted
     * in ascending order (smallest to biggest).Sort integer array in ascending order.
     * Return a new array of sorted numbers.
     * @param arr array of numbers to sort
     * @return sorted array
     */
    int[] sortNumsAscending(int[] arr) {
        boolean isSorted;
        do {
            isSorted = true;
            for (int i = 0; i < arr.length - 1; i++)
                if (arr[i] > arr[i + 1]) {
                    int aux = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = aux;
                    isSorted = false;
                }
        }
        while (!isSorted);
        return arr;
    }
}
