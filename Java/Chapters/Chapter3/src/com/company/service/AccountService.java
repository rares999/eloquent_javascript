package com.company.service;

import com.company.model.Account;

import java.math.BigDecimal;
import java.util.HashMap;

public class AccountService {
    private static AccountService accountService;
    private final HashMap<String, Account> accounts = new HashMap<>();

    private AccountService() {
    }

    public static AccountService getInstance() {
        if (accountService == null) {
            accountService = new AccountService();
        }
        return accountService;
    }

    public void printAccount(Account account) {
        if (account != null) {
            System.out.println(account.getCustomerName() + ", " + account.getAccountNumber() + ", " + account.getBalance());
        } else {
            System.out.println("Account not found!");
        }
    }

    public void openAccount(String customerName) {
        Account newAccount = new Account(customerName);
        System.out.print("Account created: ");
        printAccount(newAccount);
        accounts.put(newAccount.getAccountNumber() + "", newAccount);
    }

    public void displayAllAccounts() {
        for (Account account : accounts.values()) {
            printAccount(account);
        }
    }

    public Account searchAccount(String accountNumber) throws Exception {
        if (accounts.containsKey(accountNumber)) {
            return accounts.get(accountNumber);
        }
        throw new Exception("Account not found");
    }

    public void checkBalance(String accountNumber) throws Exception {
        Account account = searchAccount(accountNumber);
        printAccount(account);
    }

    public void addMoney(String accountNumber, BigDecimal sum) throws Exception {
        Account account = searchAccount(accountNumber);
        if (account.getBalance().add(sum).compareTo(BigDecimal.ZERO) < 0) {
            printAccount(account);
            throw new Exception("Insufficient funds");
        }
        account.addMoney(sum);
        printAccount(account);
    }

}
