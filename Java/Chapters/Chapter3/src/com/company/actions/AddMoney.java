package com.company.actions;

import com.company.service.AccountService;

import java.math.BigDecimal;

public class AddMoney implements Order {
    private AccountService service = AccountService.getInstance();
    private String accountNumber;
    private BigDecimal sum;

    public AddMoney(String accountNumber, BigDecimal sum) throws Exception {
        if (sum.compareTo(BigDecimal.ZERO) < 0)
            throw new Exception("Invalid sum");
        this.accountNumber = accountNumber;
        this.sum = sum;
    }

    @Override
    public void execute() {
        try {
            service.addMoney(accountNumber, sum);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
