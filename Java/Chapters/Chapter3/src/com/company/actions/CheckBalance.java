package com.company.actions;

import com.company.service.AccountService;

public class CheckBalance implements Order {
    private AccountService service = AccountService.getInstance();
    private String accountNumber;

    public CheckBalance(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void execute() {
        try {
            service.checkBalance(accountNumber);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
