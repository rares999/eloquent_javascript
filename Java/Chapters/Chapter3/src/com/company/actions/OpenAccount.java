package com.company.actions;

import com.company.service.AccountService;

public class OpenAccount implements Order {
    private AccountService service = AccountService.getInstance();
    private String accountNumber;

    public OpenAccount(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void execute() {
        service.openAccount(accountNumber);
    }
}
