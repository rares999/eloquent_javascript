package com.company.actions;

import com.company.model.Account;
import com.company.service.AccountService;

public class SearchAccount implements Order {
    private AccountService service = AccountService.getInstance();
    private String accountNumber;

    public SearchAccount(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Override
    public void execute() {
        try {
            Account account = service.searchAccount(accountNumber);
            service.printAccount(account);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
