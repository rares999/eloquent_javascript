package com.company.actions;

import com.company.model.Account;
import com.company.service.AccountService;

import java.math.BigDecimal;

public class WithdrawMoney implements Order {
    private AccountService service = AccountService.getInstance();
    private String accountNumber;
    private BigDecimal sum;

    public WithdrawMoney(String accountNumber, BigDecimal sum) throws Exception {
        if (sum.compareTo(BigDecimal.ZERO) < 0) {
            throw new Exception("Invalid sum");
        }
        this.accountNumber = accountNumber;
        this.sum = sum.negate();
    }

    @Override
    public void execute() {
        try {
            service.addMoney(accountNumber, sum);
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
