package com.company.actions;

import com.company.service.AccountService;

public class DisplayAccounts implements Order {
    private AccountService service = AccountService.getInstance();

    @Override
    public void execute() {
        service.displayAllAccounts();
    }
}
