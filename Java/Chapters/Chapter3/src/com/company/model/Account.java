package com.company.model;

import java.math.BigDecimal;


public class Account {
    private final String customerName;
    private final int accountNumber;
    private BigDecimal balance;
    private static int numberOfAccounts;

    public Account(String customerName) {
        this.customerName = customerName;
        balance = BigDecimal.ZERO;
        accountNumber = numberOfAccounts;
        numberOfAccounts++;
    }

    public void addMoney(BigDecimal sum) {
        this.balance = this.balance.add(sum);
    }

    public String getCustomerName() {
        return customerName;
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public BigDecimal getBalance() {
        return balance;
    }

}
