package com.company.controller;

import com.company.Banker;
import com.company.actions.*;

import java.math.BigDecimal;
import java.util.Scanner;

public class ControllerMain {

    private void printMenu() {
        System.out.println("Menu:");
        System.out.println("\t1.\tOpen account");
        System.out.println("\t2.\tDisplay all accounts");
        System.out.println("\t3.\tSearch account");
        System.out.println("\t4.\tMake deposit");
        System.out.println("\t5.\tCheck balance");
        System.out.println("\t6.\tWithdraw money");
        System.out.println("\t0.\tExit");
        System.out.println("Please choose an operation:");
    }

    public void run() {
        boolean exit = false;
        String accountNumber;
        Banker banker = new Banker();

        while (!exit) {
            banker.placeOrders();
            printMenu();
            Scanner in = new Scanner(System.in);
            String operation = in.nextLine();
            switch (operation) {
                case "0":
                    exit = true;
                    break;
                case "1":
                    System.out.println("Enter name:");
                    String name = in.nextLine();
                    OpenAccount openAccount = new OpenAccount(name);
                    banker.takeOrder(openAccount);
                    break;
                case "2":
                    DisplayAccounts displayAccounts = new DisplayAccounts();
                    banker.takeOrder(displayAccounts);
                    break;
                case "3":
                    System.out.println("Enter account number to search:");
                    accountNumber = in.nextLine();
                    SearchAccount searchAccount = new SearchAccount(accountNumber);
                    banker.takeOrder(searchAccount);
                    break;
                case "4":
                    System.out.println("Enter account number:");
                    accountNumber = in.nextLine();
                    System.out.println("Enter sum:");
                    try {
                        String sumReaded = in.nextLine();
                        BigDecimal sum = new BigDecimal(sumReaded);
                        AddMoney addMoney = new AddMoney(accountNumber, sum);
                        banker.takeOrder(addMoney);
                    } catch (Exception e) {
                        System.out.println("Invalid sum");
                    }

                    break;
                case "5":
                    System.out.println("Enter account number:");
                    accountNumber = in.nextLine();
                    CheckBalance checkBalance = new CheckBalance(accountNumber);
                    banker.takeOrder(checkBalance);
                    break;
                case "6":
                    System.out.println("Enter account number:");
                    accountNumber = in.nextLine();
                    System.out.println("Enter sum:");
                    try {
                        String sumReaded = in.nextLine();
                        BigDecimal sum = new BigDecimal(sumReaded);
                        WithdrawMoney withdrawMoney = new WithdrawMoney(accountNumber, sum);
                        banker.takeOrder(withdrawMoney);
                    } catch (Exception e) {
                        System.out.println("Invalid sum");
                    }
                    break;
                default:
                    System.out.println("Invalid operation");
                    break;

            }
        }
    }

}
