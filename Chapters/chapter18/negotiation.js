// Your code here.
let url = "https://eloquentjavascript.net/author";
let types = ["text/plain", "text/html", "application/json", "application/rainbows+unicorns"];
function negotiation() {
    for (let i = 0; i < types.length; i++) {
        fetch(url, { headers: { accept: types[i] } }).then(resp => {
            console.log(`status:${resp.status} ${types[i]}:`); resp.text().then(console.log);
        })
    }
}
negotiation();