async function locateScalpel(nest) {
    let currentNest = nest.name;
    while (1) {
        let nextNest = await anyStorage(nest, currentNest, "scalpel");
        if (nextNest == currentNest)
            return currentNest;
        currentNest = nextNest;
    }
}

function locateScalpel2(nest) {
    let currentNest = nest.name;

    function loop(currentNest) {
        return anyStorage(nest, currentNest, "scalpel").then(nextNest => {
            if (nextNest == currentNest) return currentNest;
            else return loop(nextNest);
        });
    }
    return loop(currentNest);
}

locateScalpel(bigOak).then(console.log);
// → Butcher Shop
locateScalpel2(bigOak).then(console.log);
// → Butcher Shop