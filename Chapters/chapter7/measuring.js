//Greedy Algortihm(shortest route) on each place
function yourRobot({place, parcels}, route) {
	if (route.length == 0){
    let routes = parcels.map(parcel => {
      if (parcel.place != place) {
        route = findRoute(roadGraph, place, parcel.place);
        return {route,len: route.length};
      } else {
        route = findRoute(roadGraph, place, parcel.address);
        return {route,len: route.length};
      }
    }).reduce((x, y) => x.len > y.len ? y : x);
    route = routes.route;
  }

  return {direction: route[0], memory: route.slice(1)};
}  
  runRobotAnimation(VillageState.random(), yourRobot, []);
//average number of steps: ~12