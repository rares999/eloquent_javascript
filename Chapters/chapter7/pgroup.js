class PGroup {
    constructor(group) {
      this.group = group;
    }
  
    add(value) {
      if (this.group.indexOf(value)==-1) 
      {
        let newGroup = new PGroup(this.group.concat([value]));
        return newGroup;
      }
          
      return this;
    }
  
    delete(value) {
      if (this.group.indexOf(value)==-1) 
        return this;
        let newGroup = new PGroup(this.group.filter(m => m !== value));
      return newGroup;
    }
  
    has(value)
      {
          return this.group.indexOf(value)==-1?false:true
      }
  }
  
  PGroup.empty = new PGroup([]);
  
  let a = PGroup.empty.add("a");
  let ab = a.add("b");
  let b = ab.delete("a");
  
  console.log(b.has("b"));
  // → true
  console.log(a.has("b"));
  // → false
  console.log(b.has("a"));
  // → false