let select = document.querySelector('#file');
let text = document.querySelector('#text');
let filename = document.querySelector('#filename');
let save = document.querySelector('#save');
let deletee = document.querySelector('#delete');
let root = "http://localhost:8000";

function load() {
    fetch(`${root}/`).then(res => {
        return res.text();
    }).then(text => {
        const files = text.split('\n');
        select.textContent = "";
        let choose = document.createElement("option");
        choose.textContent = "Choose value";
        choose.value = "";
        select.appendChild(choose);
        files.forEach((file, i) => {
            let option = document.createElement("option");
            option.textContent = file;
            select.appendChild(option);
            if (file == filename.value)
                select.selectedIndex = i + 1;
        });
    });
}
load();

select.addEventListener("change", e => {
    if (select.value) {
        filename.value = select.value;
        fetch(`${root}/${select.value}`).then(res => {
            res.text().then(t => {
                text.textContent = t;
            });
        });
    }
    else {
        filename.value = "";
        text.textContent = "";
    }
});

save.addEventListener("click", e => {
    e.preventDefault();
    if (filename.value) {
        fetch(`${root}/${filename.value}`, {
            method: "PUT",
            body: text.value,
            headers: {
                "Content-Type": "text/plain"
            }
        }).then(load);
    }
    else {
        console.log("invalid filename");
    }
});

deletee.addEventListener("click", e => {
    e.preventDefault();
    if (filename.value) {
        fetch(`${root}/${filename.value}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "text/plain"
            }
        }).then(load());
    }
    else {
        console.log("invalid filename");
    }
});


