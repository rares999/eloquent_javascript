specialForms.set = (args, scope) => {
    // Your code here.
    if (args.length != 2 || args[0].type != "word") {
      throw new SyntaxError("Incorrect use of set");
   };
   let value = evaluate(args[1],scope);
   while(scope)
   {
        if(Object.prototype.hasOwnProperty.call(scope,args[0].name))
        { 
           scope[args[0].name] = value;
            return value;
        }
        scope = Object.getPrototypeOf(scope)
   }
   throw new ReferenceError(`Some kind of ReferenceError`);
}
  
  run(`
  do(define(x, 4),
     define(setx, fun(val, set(x, val))),
     setx(50),
     print(x))
  `);
  // → 50
  run(`set(quux, true)`);
  // → Some kind of ReferenceError