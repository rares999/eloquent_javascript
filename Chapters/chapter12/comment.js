// This is the old skipSpace. Modify it...
function skipSpace(string) {
    let comment = string.match(/^(\s|#.*)*/);
    //console.log(string);
    //console.log(comment);
    return string.slice(comment[0].length);
  }
  
  console.log(parse("# hello\nx"));
  // → {type: "word", name: "x"}
  
  console.log(parse("a # one\n   # two\n()"));
  // → {type: "apply",
  //    operator: {type: "word", name: "a"},
  //    args: []}