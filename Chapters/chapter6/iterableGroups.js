// Your code here (and the code from the previous exercise)
class Group {
    constructor()
    {
        this.group=[];
    }
    add(value)
    {
        if(this.group.indexOf(value)==-1)
            this.group.push(value);
    }
    delete(value)
    {
        this.group = this.group.filter(values=>values!=value);
    }
    has(value)
    {
        return this.group.indexOf(value)==-1?false:true
    }
    static from(iterableObject)
    {
        let newGroup = new Group;
        for(let iterator of iterableObject)
        {
            newGroup.add(iterator);
        }
        return newGroup;
    }
    [Symbol.iterator]=function(){
        return new GroupIterator(this);    }
  }
class GroupIterator
{
    constructor(group)
    {
        this.x=0;
        this.group=group;
    }
    next()
    {
        if(this.x==this.group.group.length)
            return {done:true};
        let val =this.group.group[this.x];
        this.x++;
        return {value:val,done:false};
    }
}
for (let value of Group.from(["a", "b", "c"])) {
    console.log(value);
  }
  // → a
  // → b
  // → c