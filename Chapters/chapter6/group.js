class Group {
    constructor()
    {
        this.group=[];
    }
    add(value)
    {
        if(this.group.indexOf(value)==-1)
            this.group.push(value);
    }
    delete(value)
    {
        this.group = this.group.filter(values=>values!=value);
    }
    has(value)
    {
        return this.group.indexOf(value)==-1?false:true
    }
    static from(iterableObject)
    {
        let newGroup = new Group;
        for(let iterator of iterableObject)
        {
            newGroup.add(iterator);
        }
        return newGroup;
    }
  }
  
  let group = Group.from([10, 20]);
  console.log(group.has(10));
  // → true
  console.log(group.has(30));
  // → false
  group.add(10);
  group.delete(10);
  console.log(group.has(10));
  // → false