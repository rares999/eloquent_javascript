function countBs(x) {
    let counter = 0;
    for (let i = 0; i < x.length; i++) {
        if (x[i] == "B")
            counter++;
    }
    return counter;
}

function countChar(x, c) {
    let counter = 0;
    for (let i = 0; i < x.length; i++) {
        if (x[i] == c)
            counter++;
    }
    return counter;
}

function countBs2(x) {
    return countChar(x, "B");
}