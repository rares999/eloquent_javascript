let size = 8;
let chess = "";
for (let i = 0; i < size; i++, chess += "\n")
    for (let j = 0; j < size; j++) {
        if ((i + j) % 2 == 0)
            chess += " ";
        else
            chess += "#";
    }
console.log(chess);