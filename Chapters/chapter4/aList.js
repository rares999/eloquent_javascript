function arrayToList(array) {
    let list = null;
    for (let i = array.length - 1; i >= 0; i--) {
        list = { value: array[i], rest: list };
    }
    return list;
}

function listToArray(list) {
    let array = [];
    while (list) {
        array.push(list.value);
        list = list.rest;
    }
    return array;
}

function prepend(elem, list) {

    return { value: elem, rest: list };
}

function nth(list, nr) {
    if (nr == 0)
        return list.value;
    if (!list.rest)
        return undefined;
    return nth(list.rest, nr - 1);
}