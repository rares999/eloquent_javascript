function deepEqual(obj1, obj2) {
    if (obj1 === obj2)
        return true;
    if (obj1 == null || obj2 == null || typeof(obj1) != "object" || typeof(obj2) != "object")
        return false;
    if (Object.keys(obj1).length != Object.keys(obj2).length)
        return false;
    for (let key of Object.keys(obj1)) {
        if (!Object.keys(obj2).includes(key) || !deepEqual(obj1[key], obj2[key]))
            return false;
    }
    return true;

    let x = {
        asd: 1

    }
}