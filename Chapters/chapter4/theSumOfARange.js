function range(x, y, z) {
    let list = [];
    if (z == undefined) {
        z = 1;
        if (x > y)
            y = -1;
    }
    if (x < y)
        for (var i = x; i <= y; i += z) {
            list.push(i);
        }
    else
        for (var i = x; i >= y; i += z) {
            list.push(i);
        }
    return list;
}

function sum(list) {
    let sum = 0;
    for (let i = 0; i < list.length; i++)
        sum += list[i];
    return sum;
}