// Your code here.
function loop(start,filter,increment,action)
{
  for(let nr = start;filter(nr);nr=increment(nr))
    action(nr);
}
loop(3, n => n > 0, n => n - 1, console.log);
// → 3
// → 2
// → 1