function every(array, test) {
  for(let x of array)
  {
    if(!test(x))
      return false;
  }
  return true;
}
function everyWithSome(array,test) {
  return !array.some((array1)=>!test(array1));
}

console.log(every([1, 3, 5], n => n < 10));
// → true
console.log(every([2, 4, 16], n => n < 10));
// → false
console.log(every([], n => n < 10));
// → true
console.log(everyWithSome([1, 3, 5], n => n < 10));
// → true
console.log(everyWithSome([2, 4, 16], n => n < 10));
// → false
console.log(everyWithSome([], n => n < 10));
// → true